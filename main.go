package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	//"sync"
	"io/ioutil"
	"net/http"
	"time"

	//"k8s.io/apimachinery/pkg/api/errors"
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	//
	// Uncomment to load all auth plugins
	// _ "k8s.io/client-go/plugin/pkg/client/auth"
	//
	// Or uncomment to load specific auth plugins
	// _ "k8s.io/client-go/plugin/pkg/client/auth/azure"
	// _ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	// _ "k8s.io/client-go/plugin/pkg/client/auth/oidc"
	// _ "k8s.io/client-go/plugin/pkg/client/auth/openstack"
)

const (
	LABEL_NAME       = "stat"
	LABEL_VAL        = "offline"
	STYPE_NAME       = "SERVER_TYPE_"
	STYPE_STATE_NAME = "_state"
	REDIS_CMD_KEY    = "monitor_cmds_"

	CMD_NAME_UPDATE = "update"
	CMD_NAME_SCALE  = "scale"
	CMD_NAME_DOWN   = "down"
	CMD_NAME_UP     = "up"

	CMD_STAT_START = 1
	CMD_STAT_WAIT  = 2
	CMD_STAT_END   = 3

	// 人数<=此值时可以关闭pod
	LIMIT_USER_COUNT = 0
)

// 命令队列，来自于redis队列或是std输入
type Cmd struct {
	Cmd    string
	Params []string
}
type CmdCache struct {
	Cmds []*Cmd

	// 当前正在执行的命令
	CurCmd *Cmd
	// 当前命令执行的状态
	CurStat int
	// 当前正在监控的podids
	CurPods []int32
}

func (this *CmdCache) Add(cmd string, param ...string) {
	c := &Cmd{
		Cmd:    cmd,
		Params: make([]string, 0),
	}
	c.Params = append(c.Params, param...)
	this.Cmds = append(this.Cmds, c)
	fmt.Printf("Add cmd:%v\n", c)
}

func (this *CmdCache) Pop(upcurcmd bool) *Cmd {
	sz := len(this.Cmds)
	if sz <= 0 {
		return nil
	}
	v := this.Cmds[0]
	this.Cmds[0] = nil
	this.Cmds = this.Cmds[1:]

	// 新命令
	if upcurcmd {
		this.CurCmd = v
		this.CurStat = CMD_STAT_START
		this.CurPods = make([]int32, 0)
	}

	return v
}

func (this *CmdCache) Top() []string {
	sz := len(this.Cmds)
	if sz <= 0 {
		return nil
	}
	v := this.Cmds[0]
	ret := make([]string, 0)
	ret = append(ret, v.Cmd)
	ret = append(ret, v.Params...)

	return ret
}

func (this *CmdCache) IsBusy() bool {
	return this.CurCmd != nil
}

func (this *CmdCache) SetIdle() {
	this.CurCmd = nil
	this.CurStat = CMD_STAT_END
	this.CurPods = make([]int32, 0)
}

type CmdManager struct {
	Cc map[string]*CmdCache
}

func (this *CmdManager) Add(stype string, cmd string, param ...string) {
	if _, ok := serverTypes[stype]; !ok {
		fmt.Printf("CmdManager find cmd not for me[%v, %v, %v]\n", stype, cmd, param)
		return
	}
	if _, ok := this.Cc[stype]; !ok {
		this.Cc[stype] = &CmdCache{
			Cmds:    make([]*Cmd, 0),
			CurStat: CMD_STAT_END,
			CurPods: make([]int32, 0),
		}
	}
	c, _ := this.Cc[stype]
	c.Add(cmd, param...)
}

var (
	// 当前是否运行在cluster中
	isInCluster   bool
	monitorCmdKey string
	// 支持监控的服务器类型
	serverTypes map[string]int
	//cc          CmdCache
	cm CmdManager
)

//
// 特别注意!!!
// server type 是在dockerfile中指定 ENV 变量
// server name 是yaml中将pod名称通过 ENV 暴露给进程
// pod也会将此server type 作为label
// init中的初始化定义的必须一致
// docker image中的image以 server type:version 命名, 例如 roommgr:0.0.1
//
func init() {
	// 这里根据实际要监控的服务器类型初始化
	serverTypes = make(map[string]int)
	serverTypes["game"] = 1

	cm.Cc = make(map[string]*CmdCache)

	//cc.Cmds = make([]*Cmd, 0)
	//cc.CurStat = CMD_STAT_END
	//cc.CurPods = make([]int32, 0)
}

func homeDir() string {
	if h := os.Getenv("HOME"); h != "" {
		return h
	}
	return os.Getenv("USERPROFILE") // windows
}

func initK8sClientSet() *kubernetes.Clientset {
	if isInCluster {
		config, err := rest.InClusterConfig()
		if err != nil {
			fmt.Printf("InClusterConfig Error:%v\n", err.Error())
			return nil
		}
		fmt.Printf("config:[%v]\n", config.String())
		clientset, err := kubernetes.NewForConfig(config)
		if err != nil {
			fmt.Printf("NewForConfig(1) Error:%v\n", err.Error())
			return nil
		}
		return clientset
	} else {
		var kubeconfig *string
		if home := homeDir(); home != "" {
			kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
		} else {
			kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
		}
		flag.Parse()
		// use the current context in kubeconfig
		config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
		if err != nil {
			fmt.Printf("BuildConfigFromFlags Error:%v\n", err.Error())
			return nil
		}
		fmt.Printf(config.String())
		// create the clientset
		clientset, err := kubernetes.NewForConfig(config)
		if err != nil {
			fmt.Printf("NewForConfig(2) Error:%v\n", err.Error())
			return nil
		}
		return clientset
	}
}

func loadServerPods(clientset *kubernetes.Clientset) {
	pods, err := clientset.CoreV1().Pods("").List(metav1.ListOptions{})
	if err != nil {
		fmt.Printf("List pods Error:%v\n", err.Error())
		return
	}
	fmt.Printf("[%v] pods in cluster\n", len(pods.Items))

	for _, item := range pods.Items {
		fmt.Printf("name:%v lables:%v\n", item.GetName(), item.GetLabels())
		// 只找一种类型server, 一个server只能有一个类型
		isfind := false
		stype := ""
		for st := range serverTypes {
			if ste, ok := item.GetLabels()["stype"]; ok && ste == st {
				isfind = true
				stype = st
				break
			}
		}
		if !isfind {
			continue
		}
		// 如果有label  stat:offline 表示这个server不对外开放了
		online := 1
		if st, ok := item.GetLabels()[LABEL_NAME]; ok && st == LABEL_VAL {
			// 下线的不记录
			continue
		}
		key := STYPE_NAME + stype + STYPE_STATE_NAME
		GetClient().HSet(key, item.GetName(), strconv.Itoa(online))
		fmt.Printf("key:%v name:%v\n", key, item.GetName())
	}
}

func scaleServer(cc *CmdCache, scaletype string, stype string, clientset *kubernetes.Clientset) {
	if scaletype == CMD_NAME_UP {
		scaleUp(cc, stype, clientset)
	} else if scaletype == CMD_NAME_DOWN {
		tryScaleDown(cc, stype, clientset)
	} else {
		fmt.Printf("scaleServer scaleType Error:%v\n", scaletype)
	}
}

func tryScaleDown(cc *CmdCache, stype string, clientset *kubernetes.Clientset) {
	// 缩容操作
	if cc.CurStat == CMD_STAT_START {
		// 给 pod加上offline标签
		stsets := clientset.AppsV1().StatefulSets(apiv1.NamespaceDefault)
		stset, err := stsets.Get(stype, metav1.GetOptions{})
		if err != nil {
			fmt.Printf("tryScaleDown Get Error:%v\n", err.Error())
			cc.SetIdle()
			return
		}
		var curReplica int32
		curReplica = *stset.Spec.Replicas
		if curReplica <= 1 {
			// 不能在缩了
			fmt.Printf("tryScaleDown min pods now\n")
			cc.SetIdle()
			return
		}
		podname := stype + fmt.Sprintf("-%v", curReplica-1)
		// 增加标签
		ret := labelPod(clientset, stype, podname, true)
		if ret {
			cc.CurStat = CMD_STAT_WAIT
			cc.CurPods = append(cc.CurPods, curReplica-1)
		} else {
			fmt.Printf("labelPod Error pod:%v\n", podname)
			cc.SetIdle()
			return
		}
	} else if cc.CurStat == CMD_STAT_WAIT {
		// 检查人数
		podid := cc.CurPods[0]
		podname := stype + fmt.Sprintf("-%v", podid)
		ret, count := GetPodCount(clientset, stype, podname)
		if !ret {
			fmt.Printf("GetPodCount Error\n")
		}
		if count <= LIMIT_USER_COUNT {
			scaleDown(cc, stype, clientset)
		} else {
			// 缩容操作在这里等待人数下降到预设
			// 这里要检查下命令队列里第一个是否有其他命令,如果有(非缩容命令)则退出当前缩容过程
			nextcmd := cc.Top()
			if nextcmd == nil || len(nextcmd) < 2 || (nextcmd[0] == CMD_NAME_SCALE && nextcmd[1] == CMD_NAME_DOWN) {
				return
			}
			// 存在其他命令 结束当前的scale down
			fmt.Printf("High priority cmd exist. abort current scale down cmd. nextcmd:%v\n", nextcmd)
			ret := labelPod(clientset, stype, podname, false)
			if ret {
				cc.SetIdle()
			} else {
				fmt.Printf("Abort current scale down Error. remove label failed\n")
			}
		}
	}
}

func doScaleUp(stype string, clientset *kubernetes.Clientset) {
	stsets := clientset.AppsV1().StatefulSets(apiv1.NamespaceDefault)
	stset, err := stsets.Get(stype, metav1.GetOptions{})
	if err != nil {
		fmt.Printf("scaleUp Get Error:%v\n", err.Error())
		return
	}
	var curReplica int32
	curReplica = *stset.Spec.Replicas + 1
	stset.Spec.Replicas = &curReplica
	_, err = stsets.Update(stset)
	if err != nil {
		fmt.Printf("scaleUp Update Error:%v\n", err.Error())
	} else {
		fmt.Printf("scaleUp Success: replica[%v]\n", curReplica)
	}
}

func scaleUp(cc *CmdCache, stype string, clientset *kubernetes.Clientset) {
	defer func() {
		cc.SetIdle()
	}()
	doScaleUp(stype, clientset)
}

func scaleDown(cc *CmdCache, stype string, clientset *kubernetes.Clientset) {
	defer func() {
		cc.SetIdle()
	}()

	stsets := clientset.AppsV1().StatefulSets(apiv1.NamespaceDefault)
	stset, err := stsets.Get(stype, metav1.GetOptions{})
	if err != nil {
		fmt.Printf("scaleDown Get Error:%v\n", err.Error())
		return
	}
	var curReplica int32
	curReplica = *stset.Spec.Replicas - 1
	if curReplica <= 0 {
		curReplica = 1
	}
	stset.Spec.Replicas = &curReplica
	_, err = stsets.Update(stset)
	if err != nil {
		fmt.Printf("scaleDown Update Error:%v\n", err.Error())
	} else {
		fmt.Printf("scaleDown Success: replica[%v]\n", curReplica)
	}
}

func updateVersion(cc *CmdCache, stype string, newVersion string, clientset *kubernetes.Clientset) {
	if cc.CurStat == CMD_STAT_START {
		// 开始更新, 从最大id的pod开始往下更新
		// 先把image更新掉
		stsets := clientset.AppsV1().StatefulSets(apiv1.NamespaceDefault)
		stset, err := stsets.Get(stype, metav1.GetOptions{})
		if err != nil {
			fmt.Printf("updateVersion Get Error:%v\n", err.Error())
			cc.SetIdle()
			return
		}

		var curReplica int32
		curReplica = *stset.Spec.Replicas

		var curImage string
		curImage = stset.Spec.Template.Spec.Containers[0].Image
		newImage := stype + ":" + newVersion
		stset.Spec.Template.Spec.Containers[0].Image = newImage
		_, err = stsets.Update(stset)
		if err != nil {
			fmt.Printf("updateVersion Update Error:%v\n", err.Error())
			cc.SetIdle()
			return
		}
		fmt.Printf("updateVersion Success: oldImage[%v->%v]\n", curImage, newImage)
		// 给当前pod当标签
		podname := stype + fmt.Sprintf("-%v", curReplica-1)
		ret := labelPod(clientset, stype, podname, true)
		if ret {
			cc.CurStat = CMD_STAT_WAIT
			cc.CurPods = append(cc.CurPods, curReplica-1)
		} else {
			fmt.Printf("updateVersion add label to pod[%v] Error:%v\n", podname, err.Error())
			cc.SetIdle()
			return
		}
	} else if cc.CurStat == CMD_STAT_WAIT {
		// 检查人数
		podid := cc.CurPods[0]
		podname := stype + fmt.Sprintf("-%v", podid)
		ret, count := GetPodCount(clientset, stype, podname)
		if !ret {
			fmt.Printf("updateVersion GetPodCount Error\n")
		}
		if count <= LIMIT_USER_COUNT {
			// 关闭此服, 给下一个podid打标签 等待检查
			shutdownPod(podname, clientset)
			if podid == 0 {
				fmt.Printf("Update all pod done\n")
				cc.SetIdle()
				return
			}
			nextpodname := stype + fmt.Sprintf("-%v", podid-1)
			fmt.Printf("Update pod[%v] done. try next[%v]\n", podname, nextpodname)
			ret = labelPod(clientset, stype, nextpodname, true)
			if ret {
				cc.CurPods[0] = podid - 1
			} else {
				fmt.Printf("Update add label pod[%v] Error\n", nextpodname)
				cc.SetIdle()
				return
			}
		} else {
			// 更新过程在等服务器人数下降，这里检查下如果 扩容命令存在的话 直接执行
			nextcmd := cc.Top()
			if nextcmd == nil || len(nextcmd) < 2 || !(nextcmd[0] == CMD_NAME_SCALE && nextcmd[1] == CMD_NAME_UP) {
				return
			}
			// 存在其他命令 结束当前的scale down
			c := cc.Pop(false)
			fmt.Printf("UpdateVersion find scale up cmd[%v, %v]. run it\n", c.Cmd, c.Params)
			doScaleUp(c.Params[1], clientset)
		}
	}
}

func shutdownPod(podname string, clientset *kubernetes.Clientset) {
	fmt.Printf("shutdownPod Pod:%v\n", podname)
	pods := clientset.CoreV1().Pods(apiv1.NamespaceDefault)
	err := pods.Delete(podname, &metav1.DeleteOptions{})
	if err != nil {
		fmt.Printf("shutdownPod Pod:%v Error:%v\n", podname, err.Error())
	}
}

// 给指定pod添加删除 offline标签
func labelPod(clientset *kubernetes.Clientset, stype string, podname string, isoffline bool) bool {
	pods := clientset.CoreV1().Pods(apiv1.NamespaceDefault)
	pod, err := pods.Get(podname, metav1.GetOptions{})
	if err != nil {
		fmt.Printf("labelPod Get Pod:%v offline:%v Error:%v\n", podname, isoffline, err.Error())
		return false
	}
	if isoffline {
		pod.Labels[LABEL_NAME] = LABEL_VAL
	} else {
		delete(pod.Labels, LABEL_NAME)
	}
	_, err = pods.Update(pod)
	if err != nil {
		fmt.Printf("labelPod Update Error:%v\n", err.Error())
		return false
	} else {
		fmt.Printf("labelPod Update Success: pod[%v] isoffline[%v]\n", podname, isoffline)

		// 添加offline标签后要删缓存
		if isoffline {
			// 删除redis缓存
			key := STYPE_NAME + stype + STYPE_STATE_NAME
			GetClient().HDel(key, podname)
			fmt.Printf("delete key:%v podname:%v\n", key, podname)
		} else {
			// 删除标签时不需要写redis，redis中数据在定时拉取pod时会重新写入
		}

		return true
	}
}

// 获取pod上人数
func GetPodCount(clientset *kubernetes.Clientset, stype string, podname string) (bool, int) {
	key := STYPE_NAME + stype
	data, err := GetClient().HGet(key, podname).Result()
	if err != nil {
		fmt.Printf("GetPodCount Get Count:[%v,%v] Error:%v\n", key, podname, err.Error())
		return false, 0
	}
	type serverinfo struct {
		Ver   string `json:"ver"`
		Count int    `json:"count"`
	}
	sinfo := serverinfo{}
	if err = json.Unmarshal([]byte(data), &sinfo); err != nil {
		fmt.Printf("GetPodCount Unmarshal:%v Error:%v\n", data, err.Error())
		return false, 0
	}
	return true, sinfo.Count
}

func execCmd(cc *CmdCache, cmd *Cmd, clientset *kubernetes.Clientset) {
	// 新开始一个命令, 设置状态
	fmt.Printf("execCmd cmd:%v\n", cmd.Cmd)
	cc.CurStat = CMD_STAT_START
	runCmd(cc, cc.CurCmd, clientset)
}

func checkCurCmd(cc *CmdCache, clientset *kubernetes.Clientset) {
	// 持续检查当前命令执行情况
	runCmd(cc, cc.CurCmd, clientset)
}

func runCmd(cc *CmdCache, cmd *Cmd, clientset *kubernetes.Clientset) {
	switch cmd.Cmd {
	case CMD_NAME_UPDATE:
		updateVersion(cc, cmd.Params[0], cmd.Params[1], clientset)
	case CMD_NAME_SCALE:
		scaleServer(cc, cmd.Params[0], cmd.Params[1], clientset)
	}
}

func loadCmdFromRedis() {
	// LPOP 从列表里取命令
	// 命令的写入由其他途径调用 RPUSH 写入: RPUSH monitor_cmds_monitor1 "scale up game"
	data, err := GetClient().LPop(monitorCmdKey).Result()
	fmt.Printf("try pop key[%v] cmd param[%v,%v]\n", monitorCmdKey, data, err)
	if err != nil {
		return
	}
	cmds := strings.Split(data, " ")
	if len(cmds) >= 2 {
		stype := cmds[2]
		if cmds[0] == CMD_NAME_UPDATE {
			stype = cmds[1]
		}
		cm.Add(stype, cmds[0], cmds[1], cmds[2])
	} else {
		fmt.Printf("try add cmd[%v] Error\n", data)
	}
}

func mainLoop(clientset *kubernetes.Clientset) {
	for {
		timeStart := time.Now().UnixNano() / 1000000
		// 5秒左右执行一次
		// 1 定时get pods，将pods状态写入redis
		loadServerPods(clientset)

		// 2 从redis的命令队列取命令 TODO
		loadCmdFromRedis()

		// 3 执行命令 包括 更新、扩容、缩容
		// 要对每一组类型服务器列表执行
		for _, cc := range cm.Cc {
			if cc == nil {
				continue
			}
			if !cc.IsBusy() {
				c := cc.Pop(true)
				if c != nil {
					execCmd(cc, c, clientset)
				}
			} else {
				// 如果是更新或是缩容 需要持续监控
				//fmt.Printf("main loop busy\n")
				checkCurCmd(cc, clientset)
			}
		}

		timeEnd := time.Now().UnixNano() / 1000000
		gap := timeEnd - timeStart
		// 4 sleep
		timeSleep := 5000 - gap
		//fmt.Printf("main sleep:%v\n", timeSleep)
		time.Sleep(time.Duration(timeSleep) * time.Millisecond)
	}
}

func main() {
	// 获取monitor名称
	mname := os.Getenv("MONITOR_NAME")
	if mname == "" {
		mname = "default"
	}
	monitorCmdKey = REDIS_CMD_KEY + mname

	// 根据环境变量判断当前应该是in-cluster 还是 out-cluster
	incluster := os.Getenv("MONITOR_IN_CLUSTER")
	if incluster == "1" {
		isInCluster = true
	}

	clientset := initK8sClientSet()
	if clientset == nil {
		panic("clientset nil")
	}
	GetClient().Set("TestGameServerList", "abc", 0)
	fmt.Printf("ServerTypes:%v\n", serverTypes)

	// 定时获取相关server的信息，记入redis中
	go mainLoop(clientset)

	// for test
	for {
		time.Sleep(60 * time.Second)
		lobbyinfo := GetClient().Get("LobbyInfo").Val()
		fmt.Printf("====> lobbyinfo:%v\n", lobbyinfo)
		values := strings.Split(lobbyinfo, "|")
		if len(values) > 1 {
			url := "http://" + values[1] + "/idx?str=abc"
			request, err := http.Get(url)
			if err != nil {
				fmt.Printf("http get[%v] error[%v]\n", url, err.Error())
			} else {
				body, err := ioutil.ReadAll(request.Body)
				if err != nil {
					fmt.Printf("read error: [%v]\n", err.Error())
				} else {
					fmt.Printf("http get[%v] ret[%v]\n", url, string(body))
				}
				request.Body.Close()
			}
		}
		/*
			var (
				cmd    string
				param1 string
				param2 string
			)
			fmt.Scanln(&cmd, &param1, &param2)
			switch cmd {
			case "exit":
				break
			}
			fmt.Printf("try add cmd:%v param[%v,%v]\n", cmd, param1, param2)
			stype := param2
			if cmd == CMD_NAME_UPDATE {
				stype = param1
			}
			cm.Add(stype, cmd, param1, param2)
		*/
	}
}
