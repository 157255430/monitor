module gitlab.bianfeng.com/gdmj/gameserver

go 1.14

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/aliyun/alibaba-cloud-sdk-go v1.61.29 // indirect
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/golang/protobuf v1.4.3 // indirect
	github.com/gorilla/websocket v1.4.0 // indirect
	github.com/huandu/xstrings v1.3.2 // indirect
	github.com/importcjj/sensitive v0.0.0-20200106142752-42d1c505be7b // indirect
	github.com/jinzhu/gorm v1.9.16 // indirect
	github.com/lestrrat-go/file-rotatelogs v2.4.0+incompatible // indirect
	github.com/lestrrat-go/strftime v1.0.4 // indirect
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7 // indirect
	github.com/orcaman/concurrent-map v0.0.0-20190826125027-8c72a8bb44f6 // indirect
	github.com/pquerna/ffjson v0.0.0-20190930134022-aa0246cd15f7 // indirect
	github.com/stretchr/testify v1.5.1 // indirect
	go.uber.org/zap v1.14.0 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0 // indirect
	k8s.io/api v0.17.14
	k8s.io/apimachinery v0.17.14
	k8s.io/client-go v0.17.14
)

//require (
//	github.com/go-redis/redis v6.15.7+incompatible
//	github.com/golang/protobuf v1.3.4
//	github.com/gorilla/websocket v1.4.1
//	github.com/huandu/xstrings v1.3.0
//	github.com/jinzhu/gorm v1.9.12
//	github.com/json-iterator/go v1.1.9
//	github.com/onsi/ginkgo v1.12.0 // indirect
//	github.com/onsi/gomega v1.9.0 // indirect
//	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
//	github.com/pquerna/ffjson v0.0.0-20190930134022-aa0246cd15f7
//)
