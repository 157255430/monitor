FROM golang:latest as builder

# $GOPATH = /go
WORKDIR /go/src/monitor
ENV GO15VENDOREXPERIMENT 1
COPY . .
#RUN go get github.com/astaxie/beego && go get github.com/go-sql-driver/mysql && \
#    go get github.com/gomodule/redigo/redis && \
#    go get github.com/jinzhu/gorm

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o monitor .

FROM alpine:3.11
#ENV ENV_REACTOR test1
ENV MONITOR_IN_CLUSTER 1
ENV MONITOR_NAME monitor1
WORKDIR /
COPY --from=builder /go/src/monitor/monitor .
CMD ["./monitor"]
